import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Skill } from './skills.model';
import { simulateApiCall } from 'src/app/util/simulate-api-call.operator';

@Injectable()
export class SkillService {

  constructor(private readonly http: HttpClient) {}

  getSkills(): Observable<Skill[]> {
    return this.http.get<Skill[]>(`/data/skills.json`)
      .pipe(simulateApiCall());
  }
}
