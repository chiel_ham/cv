export interface Picture {
  base64: string;
  alt: string;
}
