export interface Personalia {
  emailAddress: string;
  linkedIn: string;
  location: string;
  phoneNumber: string;
  birthDate: Date;
}
