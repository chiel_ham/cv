import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';

import { Personalia } from './personalia.model';
import { simulateApiCall } from 'src/app/util/simulate-api-call.operator';

@Injectable()
export class PersonaliaService {

  constructor(private readonly http: HttpClient) {}

  getPersonalia(): Observable<Personalia> {
    return this.http.get<Personalia>(`/data/personalia.json`)
      .pipe(
        simulateApiCall(),
        tap(res => res.birthDate = new Date(res.birthDate))
      );
  }
}
